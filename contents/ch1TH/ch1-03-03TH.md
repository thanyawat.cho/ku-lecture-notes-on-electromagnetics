
|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |   

|[ก่อนหน้า](ch1-03-02TH.md)| [ต่อไป](ch1-04TH.md) |
| ---------- | ---------- |    

### 1.3.3 [ตัวอย่างการหาปริมาตรครึ่งทรงกลม](ch1-03-03TH.md)  
จงหาปริมาตรของครึ่งทรงกลมที่มีรัศมีเท่ากับ $`a`$  
#### วิธีทำ  
สมการพื้นผิวทรงกลมที่มีจุดศูนย์กลางอยู่ที่จุดกำเนิดและมีรัศมีเท่ากับ $`a`$ คือ $`x^2+y^2+z^2=a^2`$ 
เราให้ปริมาตรดิฟเฟอเรนเชียล หรือปริมาตรที่เล็กที่สุด $`dv = dxdydz`$  
ถ้าเราแบ่งปริมาตรของครึ่งทรงกลมเป็นกล่องสี่เหลี่ยมเล็กๆขนาดเท่ากับ $`\Delta v_m = \Delta x_i \Delta y_j \Delta z_k`$ ซึ่งนับได้ $`N`$ ชิ้น แล้วเอาปริมาตรของกล่องสี่เหลี่ยมแต่ละชิ้นมาร่วมกันทั้งหมด เราจะได้ปริมาตรของครึ่งทรงกลมโดยประมาณ  
```math
V \approx \sum_{m=1}^{N} {\Delta v_m} = \sum_{i=1}^{i=N_x} \sum_{j=y_{start(i)}}^{j= y_{stop(i)}} \sum_{k=z_{start(i,j)}}^{k= z_{stop(i,j)}} { \Delta z_k \Delta y_j \Delta x_i}
```   
ค่าของ $`y_{start}`$ และ $`y_{stop}`$ ขึ้นอยู่กับค่า $`i`$ และ $`z_{start}`$ และ $`z_{stop}`$ ขึ้นอยู่กับค่า $`i,j`$ โดยที่ $`z_{start}`$ จะเป็นระนาบ $`xy`$ และ $`z_{stop}`$ จะเป็นพื้นผิวทรงกลม  
ปริมาตรที่ได้จะต่างจากค่าจริงเพราะกล่องสี่เหลี่ยมไม่สามารถเก็บรายละเอียดของผิวโค้งได้ ผิวโค้งจะมีขรุขระเป็นขั้นบันได ถ้าจะให้ได้ค่าปริมาตรที่ใกล้เคียงกับค่าจริงมากขึ้นเราต้องแบ่ง $`\Delta x_i`$ $`\Delta y_j`$ และ $`\Delta z_k`$ ให้มีขนาดที่เล็กลง ถ้า $`\Delta x_i`$ $`\Delta y_j`$ และ $`\Delta z_k`$ มีขนาดที่เล็กที่สุดที่จะเล็กได้หรือมีขนาดเท่ากับความยาวดิฟเฟอเรนเชียล เราจะได้ขนาดของปริมาตรจริง 
```math
V = \sum_{m=1}^{m= \infty} {\Delta v_m} = \sum_{i=1}^{i=\infty} \sum_{j=y_{start(i)}}^{j= y_{stop(i)}} \sum_{k=z_{start(i,j)}}^{k= z_{stop(i,j)}} { \Delta z_k \Delta y_j \Delta x_i}
```  
ซึ่งเราไม่สามารถเอาค่ามารวมกันเป็นอนันต์ (infinite) ครั้ง แต่เราจะเปลี่ยนเครื่องหมาย summation เป็นเครื่องหมาย integrate แทนและเปลี่ยน $`{\Delta x_i}`$ เป็น $`{dx}`$ และ $`{\Delta y_j}`$ เป็น $`{dy}`$ และ $`{\Delta z_k}`$ เป็น $`{dz}`$ ดังนั้น 
```math
V = \sum_{i=1}^{i=\infty} \sum_{j=y_{start(i)}}^{j= y_{stop(i)}} \sum_{k=z_{start(i,j)}}^{j= z_{stop(i,j)}} { \Delta z_k \Delta y_j \Delta x_i}
= \int_{x=initial}^{x= final} \int_{y=g_y(x)}^{y = f_y(x)} \int_{z=g_z(x,y)}^{z = f_z(x,y)} dz dy dx
```  
โดย $`initial`$ เป็นตำแหน่ง $`i=1`$ และ $`final`$ เป็นตำแหน่ง $`i=\infty`$ และค่าของ $`g_y(x)`$ และ $`f_y(x)`$ เป็นค่าเริ่มต้นและสิ้นสุดของ $`y`$ ตามลำดับซึ่งขึ้นอยู่กับค่าของ $`x`$ ส่วนค่าของ $`g_z(x,y)`$ และ $`f_z(x,y)`$ เป็นค่าเริ่มต้นและสิ้นสุดของ $`z`$ ตามลำดับซึ่งขึ้นอยู่กับค่าของ $`x`$ และ $`y`$  
จากรูปทรงครึ่งทรงกลมเราจะได้ว่าที่ตำแหน่ง $`(x,y)`$ ใดๆค่า $`z`$ จะเปลี่ยนแปลงระหว่างระนาบ $`xy`$, $`z=0`$  และ $`z_{stop}`$ จะเป็นพื้นผิวทรงกลม $`z = \sqrt{a^2-x^2-y^2}`$ หรือ $`g_z(x,y) = 0 `$ และ $`f_z(x,y) = \sqrt{a^2-x^2-y^2}`$  และที่ค่า $`x`$ ใดๆ ค่า $`y`$ จะเปลี่ยนแปลงระหว่าง $`y= -\sqrt{a^2-x^2} `$ กับ $`y= \sqrt{a^2-x^2} `$ หรือ $`g_y(x) = -\sqrt{a^2-x^2}`$ และ $`f_y(x) = \sqrt{a^2-x^2}`$  
เราจะได้สมการปริพันธ์ดังนี้  
```math
V = \int_{x=-a}^{x= a} \int_{y=-\sqrt{a^2-x^2}}^{y = \sqrt{a^2-x^2}} \int_{z=0}^{z = \sqrt{a^2-x^2-y^2}} dz dy dx \newline  
V = \int_{x=-a}^{x= a} \int_{y=-\sqrt{a^2-x^2}}^{y = \sqrt{a^2-x^2}} \sqrt{a^2-x^2-y^2} dy dx \newline
```   
ถ้าเราคิดแค่**หนึ่งส่วนสี่**ของ**ครึ่ง**ทรงกลมหรือ**หนึ่งส่วนแปด**ของทรงกลมเราจะได้สมการเป็น
```math
\frac{V}{4} = \int_{x=0}^{x= a} \int_{y=0}^{y = \sqrt{a^2-x^2}} \sqrt{a^2-x^2-y^2} dy dx \newline
```
จาก [integral-calculator.com](https://www.integral-calculator.com/)  
```math 
\int \sqrt{a^2-x^2-y^2} dy = \frac{1}{2}\left(\left(a^2-x^2\right)\arcsin\left(\frac{y}{\sqrt{a^2-x^2}}\right)+\sqrt{a^2-x^2}y\sqrt{1-\frac{y^2}{a^2-x^2}}\right)+C
```
เราจะได้สมการเป็น  
```math
\frac{V}{4} = \int_{x=0}^{x= a} \frac{1}{2}\left(\left(a^2-x^2\right)\arcsin\left(\frac{y}{\sqrt{a^2-x^2}}\right)+\sqrt{a^2-x^2}y\sqrt{1-\frac{y^2}{a^2-x^2}}\right)\Bigg|_{y=0}^{y=\sqrt{a^2-x^2}} dx \newline
\frac{V}{4} = \int_{x=0}^{x= a} \frac{1}{2}\left(\left(a^2-x^2\right)\arcsin\left(\frac{\sqrt{a^2-x^2}}{\sqrt{a^2-x^2}}\right)+\sqrt{a^2-x^2}y\sqrt{1-\frac{a^2-x^2}{a^2-x^2}}\right)dx \newline
\frac{V}{4} = \int_{x=0}^{x= a} \frac{1}{2}\left(\left(a^2-x^2\right)\arcsin\left(1\right)+\sqrt{a^2-x^2}y\sqrt{1-1}\right) dx \newline
\frac{V}{4} = \int_{x=0}^{x= a} \frac{1}{2}\left(\left(a^2-x^2\right)\frac{\pi}{2}+\sqrt{a^2-x^2}y\sqrt{0}\right)dx \newline
\frac{V}{4} = \frac{\pi}{4}\int_{x=0}^{x= a} \left(a^2-x^2\right) dx \newline

```   
จาก [integral-calculator.com](https://www.integral-calculator.com/) $`\int_{x=0}^{x= a} \left(a^2-x^2 \right) dx = \frac{2a^3}{3}`$ เราจึงได้ว่าปริมาตรของ**ครึ่ง**ทรงกลมที่มีรัศมีเท่ากับ $`a`$ คือ 
```math
V = 4 \times \frac{\pi}{4}\int_{x=0}^{x= a} \left(a^2-x^2\right) dx \newline
V = 4 \times  \frac{\pi}{4}\frac{2a^3}{3}\newline
V = \frac{2}{3} \pi a^3\newline
```   
ตรงตามที่เราเรียนมา





|[ก่อนหน้า](ch1-03-02TH.md)| [ต่อไป](ch1-04TH.md) |
| ---------- | ---------- | 
